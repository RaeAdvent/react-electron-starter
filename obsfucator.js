/**
 * Javascript Obfuscator code https://github.com/javascript-obfuscator/javascript-obfuscator
 * this script purpose is to obsfucate some source code or whole source (recommended just a small-critical scripts)
 * for example, to obsfucate how your encrypting algorithm works, or obsfucate some secret key
 */

// put your files here
const targetFiles = [
  {
    location: './confidential/',
    filename: 'secure.example.js',
  },
]

// default output folder
const output = ''

// connfiguration more : https://github.com/javascript-obfuscator/javascript-obfuscator#cli-options
const config = [
  '--target browser',
  '--compact true',
  '--options-preset default',
  '--string-array-encoding base64',
  '--unicode-escape-sequence true',
]

const { execSync } = require('child_process')

console.info('Javascript Obfuscator begin :')
targetFiles.forEach((file) => {
  const target = file.location + file.filename

  console.info(`Obsfucating ${target}...`)
  const outputPath = file.output ? file.output : output
  const outputLocation = outputPath + file.filename

  execSync(`javascript-obfuscator ${target} --output ${outputLocation} ${config.join(
    ' '
  )}
  `)
  console.info(
    `file ${outputLocation} obfucated (location relative to project's root folder)`
  )
})

console.info('Javascript Obfuscator end.')
