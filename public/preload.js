const { contextBridge } = require('electron')
const apiHandler = require('./ipc/renderer/index')

contextBridge.exposeInMainWorld('electron', {
  ...apiHandler,
})
