module.exports = {
  SOTK: require('./models/sotk'),
  SubSotk: require('./models/sub_sotk'),
  DSP: require('./models/tabel_dsp'),
  PDT: require('./models/tabel_pdt'),
  RA: require('./models/tabel_ra'),
  SPM: require('./models/tabel_spm'),
  User: require('./models/user'),
}
