const { DataTypes } = require('sequelize')
const { sequelize } = require('../db.connection')

const SPM = sequelize.define(
  'tabel_spm',
  {
    spm_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    uuid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    no_urut: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_tahun: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    spm_variabel: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    sum_spm_awal: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    spm_jan: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    sum_spm_jan: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_feb: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    sum_spm_feb: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_mar: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    sum_spm_mar: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_tw1: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_apr: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    sum_spm_apr: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_mei: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    sum_spm_mei: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_jun: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    sum_spm_jun: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_tw2: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_jul: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    sum_spm_jul: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_ags: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    sum_spm_ags: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_sep: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    sum_spm_sep: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_tw3: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_okt: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    sum_spm_okt: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_nov: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    sum_spm_nov: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_des: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    sum_spm_des: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_tw4: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    spm_sub_total: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    sotk_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    sub_sotk_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    spm_status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'Pasif',
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: 'tabel_spm',
    timestamps: true,
    updatedAt: 'updated_at',
    indexes: [
      {
        name: 'tabel_spm_uuid_unique',
        unique: true,
        fields: [{ name: 'uuid' }],
      },
    ],
  }
)

module.exports = SPM
