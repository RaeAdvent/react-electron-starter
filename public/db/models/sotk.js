const { DataTypes } = require('sequelize')
const { sequelize } = require('../db.connection')

const SOTK = sequelize.define(
  'sotk',
  {
    sotk_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    nama_sotk: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sotk_status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'Pasif',
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: 'sotk',
    timestamps: false,
  }
)
module.exports = SOTK
