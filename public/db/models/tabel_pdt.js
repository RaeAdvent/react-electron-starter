const { DataTypes } = require('sequelize')
const { sequelize } = require('../db.connection')

const PDT = sequelize.define(
  'tabel_pdt',
  {
    pdt_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    uuid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    no_urut: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    pdt_tahun: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    pdt_variabel: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: 0,
    },
    pdt_total: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    sotk_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    sub_sotk_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    pdt_status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'Pasif',
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: 'tabel_pdt',
    timestamps: true,
    updatedAt: 'updated_at',
    indexes: [
      {
        name: 'tabel_pdt_uuid_unique',
        unique: true,
        fields: [{ name: 'uuid' }],
      },
    ],
  }
)

module.exports = PDT
