const { DataTypes } = require('sequelize')
const { sequelize } = require('../db.connection')

const DSP = sequelize.define(
  'tabel_dsp',
  {
    dsp_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    uuid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    no_urut: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    dsp_tahun: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    dsp_variabel: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sum_dsp_awal: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_jan: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_dsp_jan: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_feb: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_dsp_feb: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_mar: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_dsp_mar: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_tw1: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    dsp_apr: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_dsp_apr: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_mei: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_dsp_mei: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_jun: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_dsp_jun: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_tw2: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_jul: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_dsp_jul: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_ags: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_dsp_ags: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_sep: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_dsp_sep: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_tw3: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_okt: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_dsp_okt: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_nov: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_dsp_nov: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_des: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_dsp_des: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_tw4: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    dsp_sub_total: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    sotk_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    sub_sotk_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    dsp_status: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stat_jan: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stat_feb: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stat_mar: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stat_apr: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stat_mei: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stat_jun: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stat_jul: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stat_agus: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stat_sep: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stat_okt: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stat_nov: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stat_des: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  },
  {
    sequelize,
    tableName: 'tabel_dsp',
    timestamps: true,
    updatedAt: 'updated_at',
    indexes: [
      {
        name: 'tabel_dsp_uuid_unique',
        unique: true,
        fields: [{ name: 'uuid' }],
      },
      {
        name: 'no_urut',
        fields: [{ name: 'no_urut' }],
      },
    ],
  }
)

module.exports = DSP
