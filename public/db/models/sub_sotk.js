const { DataTypes } = require('sequelize')
const { sequelize } = require('../db.connection')

const SubSotk = sequelize.define(
  'sub_sotk',
  {
    sub_sotk_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    sotk_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    nama_sub_sotk: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sub_sotk_status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'Pasif',
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: 'sub_sotk',
    timestamps: false,
  }
)

module.exports = SubSotk
