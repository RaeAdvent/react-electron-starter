const { DataTypes } = require('sequelize')
const { sequelize } = require('../db.connection')

const User = sequelize.define(
  'user',
  {
    user_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    user_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    user_nip: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sotk_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    sub_sotk_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    level: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    user_email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    user_status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'Pasif',
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: 'user',
    timestamps: false,
  }
)

module.exports = User
