const { DataTypes } = require('sequelize')
const { sequelize } = require('../db.connection')

const RA = sequelize.define(
  'tabel_ra',
  {
    ra_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    uuid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    no_urut: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    ra_tahun: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    ra_variabel: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_awal: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_jan: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_jan: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_feb: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_feb: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_mar: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_mar: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_tw1: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_apr: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_apr: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_mei: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_mei: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_jun: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_jun: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_tw2: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_jul: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_jul: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_ags: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_ags: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_sep: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_sep: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_tw3: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_okt: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_okt: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_nov: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_nov: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_des: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    sum_ra_des: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_tw4: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ra_sub_total: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    sotk_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    sub_sotk_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    ra_status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'Pasif',
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: 'tabel_ra',
    timestamps: true,
    updatedAt: 'updated_at',
    indexes: [
      {
        name: 'tabel_ra_uuid_unique',
        unique: true,
        fields: [{ name: 'uuid' }],
      },
    ],
  }
)

module.exports = RA
