const { app } = require('electron')
const { resourcesPath } = require('process')
const { Sequelize } = require('sequelize')
const path = require('path')
const isDev = require('electron-is-dev')

const dbPath = isDev
  ? path.join(app.getAppPath(), 'extraResources/database.sqlite')
  : path.join(resourcesPath, 'database.sqlite')

// sqlite3 db params
const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: dbPath,
})

const dbconnection = async () => {
  try {
    await sequelize.authenticate()
    console.log(
      '\x1b[32m',
      'Database Connection has been established successfully.'
    )
  } catch (error) {
    console.error('\x1b[31m', 'Unable to connect to the database:', error)
  }
}

module.exports = { dbconnection, sequelize }
