const testHandler = require('./test')
const sequelizeHandler = require('./sequelize')

// initialize handler
testHandler()
sequelizeHandler()
