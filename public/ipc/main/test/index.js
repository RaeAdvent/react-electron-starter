const { ipcMain } = require('electron') // electron

module.exports = () => {
  ipcMain.handle('test-invoke', (e, data) => {
    console.log('getting test invoke event')
    return 'Hello from electron!'
  })
}
