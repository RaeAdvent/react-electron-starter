const { ipcMain } = require('electron') // electron
const SequelizeModels = require('../../db/model.collection')

const checkParams = (model, options) => {
  if (!model || !options || Object.keys(options).length < 1) {
    return { status: false, message: 'empty options or model not allowed' }
  }
  return
}

module.exports = () => {
  ipcMain.handle('sqlGet', async (e, { model, options = {} }) => {
    const result = await SequelizeModels[model].findAll(options)
    return JSON.parse(JSON.stringify(result)) // return model to ipcRenderer
  })

  ipcMain.handle('sqlGetOne', async (e, { model, options = {} }) => {
    checkParams(model, options)

    const result = await SequelizeModels[model].findOne(options)
    return JSON.parse(JSON.stringify(result)) // return model to ipcRenderer
  })

  ipcMain.handle('sqlCreate', async (e, { model, options = {} }) => {
    checkParams(model, options)

    const result = await SequelizeModels[model].create(options)
    return JSON.parse(JSON.stringify(result)) // return model to ipcRenderer
  })

  ipcMain.handle('sqlBulkCreate', async (e, { model, rows, options = {} }) => {
    checkParams(model, options)

    try {
      const result = await SequelizeModels[model].bulkCreate(rows, options)
      return JSON.parse(JSON.stringify(result)) // return model to ipcRenderer
    } catch (error) {
      console.log({ msg: 'Error bulkCreate', error })
    }
  })

  ipcMain.handle('sqlDelete', async (e, { model, options = {} }) => {
    checkParams(model, options)

    const result = await SequelizeModels[model].destroy(options)
    return JSON.parse(JSON.stringify(result)) // return model to ipcRenderer
  })

  ipcMain.handle(
    'sqlUpdate',
    async (e, { model, updates = {}, options = {} }) => {
      checkParams(model, options)

      const result = await SequelizeModels[model].update(updates, options)
      return JSON.parse(JSON.stringify(result)) // return model to ipcRenderer
    }
  )

  ipcMain.handle('sqlUpsert', async (e, { model, options = {} }) => {
    checkParams(model, options)

    const result = await SequelizeModels[model].upsert(options)
    return JSON.parse(JSON.stringify(result)) // return model to ipcRenderer
  })

  ipcMain.handle('sqlFindOrCreate', async (e, { model, options = {} }) => {
    checkParams(model, options)

    const result = await SequelizeModels[model].upsert(options)
    return JSON.parse(JSON.stringify(result)) // return model to ipcRenderer
  })

  ipcMain.handle('sqlMin', async (e, { model, column, options = {} }) => {
    checkParams(model, options)

    const result = await SequelizeModels[model].min(column, options)
    return JSON.parse(JSON.stringify(result)) // return model to ipcRenderer
  })

  ipcMain.handle('sqlMax', async (e, { model, column, options = {} }) => {
    checkParams(model, options)

    const result = await SequelizeModels[model].max(column, options)
    return JSON.parse(JSON.stringify(result)) // return model to ipcRenderer
  })

  ipcMain.handle('sqlSum', async (e, { model, column, options = {} }) => {
    checkParams(model, options)

    const result = await SequelizeModels[model].sum(column, options)
    return JSON.parse(JSON.stringify(result)) // return model to ipcRenderer
  })
}
