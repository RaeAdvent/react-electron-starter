const { ipcMain } = require('electron')

const invokeHandler = (event, data) =>
  ipcMain.handle(event, async (e, data) => {})

module.exports = { invokeHandler }
