const { invoker } = require('./_handler')

module.exports = {
  sqlGet: (args) => invoker('sqlGet', args),
  sqlGetOne: (args) => invoker('sqlGetOne', args),
  sqlCreate: (args) => invoker('sqlCreate', args),
  sqlBulkCreate: (args) => invoker('sqlBulkCreate', args),
  sqlDelete: (args) => invoker('sqlDelete', args),
  sqlUpdate: (args) => invoker('sqlUpdate', args),
  sqlUpsert: (args) => invoker('sqlUpsert', args),
  sqlFindOrCreate: (args) => invoker('sqlFindOrCreate', args),
  sqlMin: (args) => invoker('sqlMin', args),
  sqlMax: (args) => invoker('sqlMax', args),
  sqlSum: (args) => invoker('sqlSum', args),
}
