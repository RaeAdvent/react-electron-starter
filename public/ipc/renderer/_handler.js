const { ipcRenderer } = require('electron')

const rendererMethod = (event, data = null) => {
  return new Promise((resolve) => {
    ipcRenderer.once(event, (_, arg) => {
      resolve(arg)
    })
    ipcRenderer.send(event, data)
  })
}

const invoker = async (event, data = null) => {
  return await ipcRenderer.invoke(event, data)
}

module.exports = { rendererMethod, invoker }
