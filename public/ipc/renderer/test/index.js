const { invoker } = require('../_handler')

module.exports = {
  // Invoke Methods
  testInvoke: (args) => invoker('test-invoke', args),
}
