const testhandler = require('./test')
const sequelizeHandler = require('./sequelize')

module.exports = {
  ...testhandler,
  ...sequelizeHandler,
}
