import React, { createContext, useContext } from 'react'
import { inject, observer } from 'mobx-react'

const Store = createContext()

export function useStore() {
  return useContext(Store)
}

function StoreProvider({ stores, children, ...props }) {
  return <Store.Provider value={stores}>{children}</Store.Provider>
}

export default inject(({ stores }) => ({ stores }))(observer(StoreProvider))
