import React, { createContext, useContext, useEffect, useState } from 'react'
import { inject, observer } from 'mobx-react'

import CssBaseline from '@material-ui/core/CssBaseline'
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'

const UiConfig = createContext()

export function useUiConfig() {
  return useContext(UiConfig)
}

function UiConfigProvider({ stores, children, ...props }) {
  const { type } = stores.uiStore
  const [themeColorMode, setThemeColorMode] = useState('light')

  const theme = createMuiTheme({
    palette: {
      type: themeColorMode,
      common: {
        black: '#000',
        white: '#fff',
      },
      primary: {
        main: '#FFFFFF',
        light: 'rgb(255, 255, 255)',
        dark: 'rgb(178, 178, 178)',
        contrastText: 'rgba(0, 0, 0, 0.87)',
      },
      secondary: {
        main: '#cdcdcd',
        light: 'rgb(215, 215, 215)',
        dark: 'rgb(143, 143, 143)',
        contrastText: 'rgba(0, 0, 0, 0.87)',
      },
      error: {
        light: '#e57373',
        main: '#f44336',
        dark: '#d32f2f',
        contrastText: '#fff',
      },
      warning: {
        light: '#ffb74d',
        main: '#ff9800',
        dark: '#f57c00',
        contrastText: 'rgba(0, 0, 0, 0.87)',
      },
      info: {
        light: '#64b5f6',
        main: '#2196f3',
        dark: '#1976d2',
        contrastText: '#fff',
      },
      success: {
        light: '#81c784',
        main: '#4caf50',
        dark: '#388e3c',
        contrastText: 'rgba(0, 0, 0, 0.87)',
      },
    },
    breakpoints: {
      values: {
        xs: 300,
        sm: 600,
        md: 960,
        lg: 1280,
        xl: 1920,
        tablet: 640,
        laptop: 1024,
        desktop: 1280,
      },
    },
  })

  useEffect(() => {
    if (type && typeof type === 'string') {
      setThemeColorMode(type)
    }
  }, [type])

  return (
    <UiConfig.Provider value={stores}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        {children}
      </MuiThemeProvider>
    </UiConfig.Provider>
  )
}

export default inject(({ stores }) => ({ stores }))(observer(UiConfigProvider))
