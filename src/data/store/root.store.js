import React from 'react'
import { Provider } from 'mobx-react'

// stores
import authStore from './AuthStore'
import uiStore from './UiStore'

export default function RootStore({ children }) {
  const stores = {
    authStore,
    uiStore,
  }
  return <Provider stores={stores}>{children}</Provider>
}
