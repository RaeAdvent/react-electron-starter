import persistence from './helper/persist'
import { types } from 'mobx-state-tree'

export const AuthStore = types
  .model({
    isLoggedIn: types.optional(types.boolean, false),
    sotkId: types.optional(types.string, ''),
    subSotkId: types.optional(types.string, ''),
    token: types.optional(types.string, ''),
    expires: types.optional(types.string, ''),
    hydrated: types.optional(types.boolean, false), // flag to indicate is state hydrated or not from localStorage
  })
  .actions((store) => ({
    afterHydration() {
      // This lifecycle is called after the store is hydrated
      store.hydrated = true
      // console.log('I feel refreshed!')
    },
    setLoggedIn({ mahasiswa, profile }) {
      store.isLoggedIn = true
      store.token = mahasiswa.token.toString()
      store.expires = ''
      store.sotkId = ''
      store.subSotkId = ''
    },
    updateToken({ token, expires }) {
      store.token = token.toString()
      store.expires = expires.toString()
      console.log('token renewed')
    },
    setLoggedOut() {
      localStorage.removeItem('authStore')
      store.isLoggedIn = false
      store.token = ''
      store.sotkId = ''
      store.subSotkId = ''
      store.expires = ''
    },
  }))

const authStore = AuthStore.create()

persistence('authStore', authStore, {
  // set which field to persist
  isLoggedIn: true,
  token: true,
  expires: true,
  sotkId: true,
  subSotkId: true,
})

export default authStore
