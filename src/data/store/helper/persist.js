import { onSnapshot, applySnapshot } from 'mobx-state-tree'
import storage from './storage'

const persistence = (name, store, schema = {}) => {
  let hydrated = false

  onSnapshot(store, (_snapshot) => {
    if (!hydrated) {
      return
    }
    const snapshot = { ..._snapshot }
    Object.keys(snapshot).forEach((key) => {
      if (!schema[key]) {
        delete snapshot[key]
      }
    })
    const data = JSON.stringify(snapshot)
    storage.setItem(name, data)
  })

  storage.getItem(name).then((data) => {
    if (data) {
      const snapshot = JSON.parse(data)
      applySnapshot(store, snapshot)
      if (store.afterHydration && typeof store.afterHydration === 'function') {
        store.afterHydration()
      }
    }
    setTimeout(() => {
      hydrated = true
    }, 500)
  })
}

export default persistence
