import persistence from './helper/persist'
import { types } from 'mobx-state-tree'

export const UiStore = types
  .model({
    type: types.optional(types.string, 'light'),
    hydrated: types.optional(types.boolean, false), // flag to indicate is state hydrated or not from localStorage
  })
  .actions((store) => ({
    afterHydration() {
      // This lifecycle is called after the store is hydrated
      store.hydrated = true
      // console.log('I feel refreshed!')
    },

    toggleThemeType() {
      store.type = store.type === 'light' ? 'dark' : 'light'
      console.log('theme changed : ' + store.type)
    },
  }))

const uiStore = UiStore.create()

persistence('uiStore', uiStore, {
  // set which field to persist
  type: true,
})

export default uiStore
