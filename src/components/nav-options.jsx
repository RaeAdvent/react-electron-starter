import { inject, observer } from 'mobx-react'

import BookOutlinedIcon from '@material-ui/icons/BookOutlined'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import ForumOutlinedIcon from '@material-ui/icons/ForumOutlined'
import HowToRegOutlinedIcon from '@material-ui/icons/HowToRegOutlined'
import LibraryBooksOutlinedIcon from '@material-ui/icons/LibraryBooksOutlined'
import { Link } from '@reach/router'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import LooksOneOutlinedIcon from '@material-ui/icons/LooksOneOutlined'
import LooksTwoOutlined from '@material-ui/icons/LooksTwoOutlined'
import MenuBookOutlinedIcon from '@material-ui/icons/MenuBookOutlined'
// public icon
import PersonOutline from '@material-ui/icons/PersonOutline'
import PowerSettingsNewOutlinedIcon from '@material-ui/icons/PowerSettingsNewOutlined'
import React from 'react'
// secure icons
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle'
import SupervisorAccountOutlinedIcon from '@material-ui/icons/SupervisorAccountOutlined'
import { useNavigate } from '@reach/router'
import { useSnackbar } from 'notistack'

const RenderDrawerList = ({ list = [] }) => (
  <List>
    {list.map((l) => {
      if (!l.url && typeof l.method === 'function') {
        return (
          <ListItem button key={l.text} onClick={l.method}>
            <ListItemIcon>{l.icon}</ListItemIcon>
            <ListItemText primary={l.text} />
          </ListItem>
        )
      }
      return (
        <ListItem
          button
          key={l.url}
          component={Link}
          to={l.url}
          className="no-underline"
        >
          <ListItemIcon>{l.icon}</ListItemIcon>
          <ListItemText primary={l.text} />
        </ListItem>
      )
    })}
  </List>
)

function NavOptions({ stores }) {
  const navigate = useNavigate()
  const { isLoggedIn, setLoggedOut } = stores.authStore
  const { enqueueSnackbar } = useSnackbar()

  const logout = async () => {
    // const { data } = await axios.get('/api/auth/logout')

    enqueueSnackbar('Anda keluar dari aplikasi', {
      variant: 'success',
    })

    setLoggedOut()
    navigate('/', { replace: true })
  }

  const publicMenuList = [
    {
      url: '/login',
      text: 'Login',
      icon: <ExitToAppIcon />,
    },
    {
      url: '/register',
      text: 'Register',
      icon: <PersonOutline />,
    },
  ]

  const secureMenuList = [
    {
      url: '/perkuliahan',
      text: 'Perkuliahan',
      icon: <MenuBookOutlinedIcon />,
    },
    {
      url: '/bimbingan-pa',
      text: 'Bimbingan PA',
      icon: <SupervisedUserCircleIcon />,
    },
    {
      url: '/bimbingan-promotor',
      text: 'Bimbingan Promotor',
      icon: <SupervisorAccountOutlinedIcon />,
    },

    {
      url: '/ujian-kolokium',
      text: 'Ujian Kolokium',
      icon: <LibraryBooksOutlinedIcon />,
    },
    {
      url: '/ujian-kualifikasi',
      text: 'Ujian Kualifikasi',
      icon: <HowToRegOutlinedIcon />,
    },
    {
      url: '/ujian-seminar-hasil',
      text: 'Ujian Seminar Hasil',
      icon: <ForumOutlinedIcon />,
    },
    {
      url: '/ujian-tahap-1',
      text: 'Ujian Tahap 1',
      icon: <LooksOneOutlinedIcon />,
    },
    {
      url: '/ujian-tahap-2',
      text: 'Ujian Tahap 2',
      icon: <LooksTwoOutlined />,
    },
    {
      url: '/usulan-penelitian',
      text: 'Usulan Penelitian',
      icon: <BookOutlinedIcon />,
    },
    {
      method: () => logout(),
      text: 'Keluar Aplikasi',
      icon: <PowerSettingsNewOutlinedIcon color="error" />,
    },
  ]

  return isLoggedIn ? (
    <RenderDrawerList list={secureMenuList} />
  ) : (
    <RenderDrawerList list={publicMenuList} />
  )
}

export default inject(({ stores }) => ({ stores }))(observer(NavOptions))
