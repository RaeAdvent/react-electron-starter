import CssBaseline from '@material-ui/core/CssBaseline'
import Navbar from './navbar'
import Navigation from './navigation'
import React from 'react'
import Routes from 'routes'
import { useLayoutStyles } from 'styles/layout.styles'

export default function Layout() {
  const classes = useLayoutStyles()
  const [open, setOpen] = React.useState(false)

  const handleDrawerOpen = () => {
    setOpen(true)
  }

  const handleDrawerClose = () => {
    setOpen(false)
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Navbar {...{ classes, open, handleDrawerOpen }} />
      <Navigation {...{ classes, open, handleDrawerClose }} />
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Routes />
        <footer className={classes.bottomBar}></footer>
      </main>
    </div>
  )
}
