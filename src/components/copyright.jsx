import React from 'react'
import Link from '@material-ui/core/Link'
import Typography from '@material-ui/core/Typography'

export default function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://langkibo.com/" target="_blank">
        Langkibo
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  )
}

export function CopyrightDrawer() {
  return (
    <Typography variant="caption" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://langkibo.com/" target="_blank">
        Langkibo
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  )
}
