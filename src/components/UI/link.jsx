import Link from '@material-ui/core/Link'
import { Link as ReachLink } from '@reach/router'
import React from 'react'

export default function CustomLink({ children, to, ...props }) {
  return (
    <Link
      className="no-underline"
      component={ReachLink}
      to={to}
      color="inherit"
      {...props}
    >
      {children}
    </Link>
  )
}
