import AppBar from '@material-ui/core/AppBar'
import Brightness4Icon from '@material-ui/icons/Brightness4'
import BrightnessHighIcon from '@material-ui/icons/BrightnessHigh'
import IconButton from '@material-ui/core/IconButton'
import Link from 'components/UI/link'
import MenuIcon from '@material-ui/icons/Menu'
import NetworkCheckOutlinedIcon from '@material-ui/icons/NetworkCheckOutlined'
import PermScanWifiOutlinedIcon from '@material-ui/icons/PermScanWifiOutlined'
import React from 'react'
import Toolbar from '@material-ui/core/Toolbar'
import Tooltip from '@material-ui/core/Tooltip'
import Typography from '@material-ui/core/Typography'
import bkyLogo from 'assets/images/bky-logo.png'
import clsx from 'clsx'
import uiStore from 'data/store/UiStore'
import { useOnlineState } from 'beautiful-react-hooks'

export default function Navbar({ classes, open, handleDrawerOpen }) {
  const { type, toggleThemeType } = uiStore
  const isOnline = useOnlineState()

  return (
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: open,
      })}
    >
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleDrawerOpen}
          edge="start"
          className={clsx(classes.menuButton, {
            [classes.hide]: open,
          })}
        >
          <MenuIcon />
        </IconButton>
        <Link
          to="/"
          color="inherit"
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'left',
            alignItems: 'center',
          }}
          className="no-underline"
        >
          <img
            src={bkyLogo}
            alt="bengkayang"
            width={30}
            style={{ marginRight: 3 }}
          />
          <Typography variant="h6" className="no-underline" noWrap>
            SIMDAKES
          </Typography>
        </Link>

        <div className={classes.rightMenu}>
          <Tooltip title="Ganti Tema">
            <IconButton
              color="inherit"
              aria-label="change theme"
              onClick={toggleThemeType}
              edge="end"
            >
              {type === 'light' ? (
                <Brightness4Icon color="inherit" />
              ) : (
                <BrightnessHighIcon color="inherit" />
              )}
            </IconButton>
          </Tooltip>

          <IconButton color="inherit" aria-label="isOnlint" edge="end">
            {isOnline ? (
              <Tooltip title="Online">
                <NetworkCheckOutlinedIcon color="inherit" />
              </Tooltip>
            ) : (
              <Tooltip title="Offline">
                <PermScanWifiOutlinedIcon color="inherit" />
              </Tooltip>
            )}
          </IconButton>
        </div>
      </Toolbar>
    </AppBar>
  )
}
