import './index.css'

import App from './App.jsx'
import React from 'react'
import ReactDOM from 'react-dom'
import RootStore from 'data/store/root.store'
import Slide from '@material-ui/core/Slide'
import { SnackbarProvider } from 'notistack'
import StoreProvider from 'providers/StoreProvider'

const AppEntry = () => {
  return (
    <SnackbarProvider
      style={{ marginTop: 50 }}
      autoHideDuration={3000}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      TransitionComponent={Slide}
      preventDuplicate
    >
      <RootStore>
        <StoreProvider>
          <App />
        </StoreProvider>
      </RootStore>
    </SnackbarProvider>
  )
}

ReactDOM.render(<AppEntry />, document.getElementById('root'))
