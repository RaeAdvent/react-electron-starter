import {
  LocationProvider,
  createHistory,
  createMemorySource,
} from '@reach/router'

import Layout from 'components/layout'
import UiConfigProvider from 'providers/UiConfigProvider'

// necessary for electron
let source = createMemorySource('/')
let history = createHistory(source)

function App() {
  return (
    <UiConfigProvider>
      <LocationProvider history={history}>
        <Layout />
      </LocationProvider>
    </UiConfigProvider>
  )
}

export default App
