import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import Container from '@material-ui/core/Container'
import Divider from '@material-ui/core/Divider'
// import { Link } from '@reach/router'
import React from 'react'
import Typography from '@material-ui/core/Typography'

const index = (props) => {
  return (
    <Container maxWidth="md">
      <Card>
        <CardHeader
          title={
            <>
              <Typography variant="h3" color="textPrimary">
                SIMDAKES Desktop
              </Typography>
              <Divider />
            </>
          }
        />
        <CardContent></CardContent>
      </Card>
    </Container>
  )
}

export default index
