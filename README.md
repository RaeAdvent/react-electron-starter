# ELECTRON.JS - REACT STARTER KIT

## ABOUT

- This starter pack installed with react and some basic routing using @reach/router history router, mobx for state management and tailwindcss for styling.

- This pack also provide a simple script for source code obsfucating using [javascript-obfuscator](https://github.com/javascript-obfuscator/javascript-obfuscator) to protect your source code, you configure which file to obsfucate (it is recommended to only obsfucate a certain script only because obsfucating a script can result on increasing file size)

```javascript
// configuring your files
const targetFiles = [
  {
    location: './confidential/', // folder location of your script relative to project's root folder
    filename: 'secure.example.js', // your target file to obsfucate
    output: './obs/', // output folder after obsfucating, default to project's root folder if not exist
  },
  ...otherFiles,
]
```

then just run ## npm run obsfucate to obsfucate your code
